import sublime, sublime_plugin, subprocess, os

class RunPowershellCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = self.window.folders()[0]
    subprocess.Popen(r"C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe", cwd = filepath)

class RunPowershellFileCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = os.path.dirname(self.window.active_view().file_name())
    subprocess.Popen(r"C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe", cwd = filepath)

class RunTortoiseGitSyncCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = self.window.folders()[0]
    subprocess.Popen(r"C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe  /command:sync", cwd = filepath)

class RunTortoiseGitSyncFileCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = os.path.dirname(self.window.active_view().file_name())
    subprocess.Popen(r"C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe  /command:sync", cwd = filepath)

class RunTortoiseGitCommitCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = self.window.folders()[0]
    subprocess.Popen(r"C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe  /command:commit", cwd = filepath)

class RunTortoiseGitCommitFileCommand(sublime_plugin.WindowCommand):
  def run(self):
    filepath = os.path.dirname(self.window.active_view().file_name())
    subprocess.Popen(r"C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe  /command:commit", cwd = filepath)